<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('Notifications_model');
	}

	public function getNotifications(){
		$sessionID=$this->input->post('sessionID');

		$this->Notifications_model->getNotifications($sessionID);
	}

	public function sync(){
		$sessionID=$this->input->post('sessionID');

		echo json_encode(array('num'=>0),JSON_FORCE_OBJECT);
	}
}


?>