<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Diary extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Diary_model');
	}


	public function getDiarySnippets(){
		$sessionID=$this->input->post('sessionID');

		$this->Diary_model->getDiarySnippets($sessionID);
	}

	public function addToDiary(){
		$sessionID=$this->input->post('sessionID');
		$title=$this->input->post('title');
		$category=$this->input->post('category');
		$content=$this->input->post('content');

		date_default_timezone_set('America/Los_Angeles');

		$date = date('Y-m-d', time());
		$time =date('H:i:s',time());

		$this->Diary_model->addToDiary($sessionID,$title,$category,$content,$date,$time);
	}

	public function editDiarySnippet(){
		$sessionID=$this->input->post('sessionID');
		$title=$this->input->post('title');
		$category=$this->input->post('category');
		$content=$this->input->post('content');
		$snippetID=$this->input->post('snippetID');

		$this->Diary_model->editDiarySnippet($sessionID,$title,$category,$content,$snippetID);
	}

	public function deleteDiarySnippet(){
		$sessionID=$this->input->post('sessionID');
		$snippetID=$this->input->post('snippetID');

		$this->Diary_model->deleteDiarySnippet($sessionID,$snippetID);
	}
}