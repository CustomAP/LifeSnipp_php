<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Extras extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Extras_model');
	}

	public function getCategories(){
		$sessionID=$this->input->post('sessionID');

		$this->Extras_model->getCategories($sessionID);
	}

	public function checkUpdate(){
		$sessionID=$this->input->post('sessionID');
		$appVersion=$this->input->post('appVersion');

		$this->Extras_model->checkUpdate($sessionID,$appVersion);
	}

	// public function deleteLogs(){
	// 	$q=$this->db->where('data','NULL')
	// 				->delete('ci_sessions');
	// }

	// public function enctdecypt(){
	// 	$this->load->library('encryption');

	// 	$pw='ashish';

	// 	echo $this->encryption->encrypt($pw);
	// }
}