<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_Profile extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('User_profile_model');
	}

	public function getSelfProfileInfo(){
		$sessionID=$this->input->post('sessionID');

		$this->User_profile_model->getSelfProfileInfo($sessionID);	
	}

	public function getOthersProfileInfo(){
		$sessionID=$this->input->post('sessionID');
		$userID=$this->input->post('userID');

		$this->User_profile_model->getOthersProfileInfo($sessionID,$userID);
	}

	public function addBasicInfo(){
		$sessionID=$this->input->post('sessionID');
		$gender=$this->input->post('gender');
		$country=$this->input->post('country');
		$birthDate=$this->input->post('birthDate');
		$firstName=$this->input->post('firstName');
		$lastName=$this->input->post('lastName');

		$this->User_profile_model->addBasicInfo($sessionID,$gender,$country,$birthDate,$firstName,$lastName);
	}

	public function follow(){
		$sessionID=$this->input->post('sessionID');
		$userID=$this->input->post('userID');

		$this->User_profile_model->follow($sessionID,$userID);
	}

	public function unfollow(){
		$sessionID=$this->input->post('sessionID');
		$userID=$this->input->post('userID');

		$this->User_profile_model->unfollow($sessionID,$userID);
	}

	public function changeProfPic(){
		$sessionID=$this->input->post('sessionID');
		$image=$this->input->post('image');

		$this->User_profile_model->changeProfPic($sessionID,$image);
	}

	public function editBio(){
		$sessionID=$this->input->post('sessionID');
		$bio=$this->input->post('bio');

		$this->User_profile_model->editBio($sessionID,$bio);
	}

	public function editBirthDate(){
		$sessionID=$this->input->post('sessionID');
		$birthDate=$this->input->post('birthDate');

		$this->User_profile_model->editBirthDate($sessionID,$birthDate);
	}

	public function editDisplayName(){
		$sessionID=$this->input->post('sessionID');
		$firstName=$this->input->post('firstName');
		$lastName=$this->input->post('lastName');

		$this->User_profile_model->editDisplayName($sessionID,$firstName,$lastName);
	}

	public function getFollowers(){
		$sessionID=$this->input->post('sessionID');
		$num=$this->input->post('num');
		$userID=$this->input->post('userID');

		$this->User_profile_model->getFollowers($sessionID,$num,$userID);
	}

	public function getFollowing(){
		$sessionID=$this->input->post('sessionID');
		$num=$this->input->post('num');
		$userID=$this->input->post('userID');

		$this->User_profile_model->getFollowing($sessionID,$num,$userID);
	}

	public function searchUsers(){
		$sessionID=$this->input->post('sessionID');
		$input=$this->input->post('input');

		$this->User_profile_model->searchUsers($sessionID,$input);
	}
}

?>