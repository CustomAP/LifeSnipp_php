<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_login extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('User_login_model');
	}

	public function sign_in(){
		//$this->load->view('welcome_message');
		$username=$this->input->post('userName');
		$password=$this->input->post('password');
		$emailID=$this->input->post('emailID');

		$flag_addUser=$this->User_login_model->addUser($username,$password,$emailID);
	}

	public function addUserDeviceInfo(){
		$userID=$this->input->post('userID');
		$board=$this->input->post('board');
		$bootloader=$this->input->post('bootloader');
		$brand=$this->input->post('brand');
		$device=$this->input->post('device');
		$display=$this->input->post('display');
		$hardware=$this->input->post('hardware');
		$manufacturer=$this->input->post('manufacturer');
		$model=$this->input->post('model');
		$product=$this->input->post('product');

		$flag_addUserDeviceInfo=$this->User_login_model->addUserDeviceInfo($userID,$board,$bootloader,$brand,$device,$display,$hardware,$manufacturer,$model,$product);
	}

	public function assignSessionID(){
		$userName=$this->input->post('userName');
		$password=$this->input->post('password');
		// $userName='AshishPawar';
		// $password='ashish';

		$userID=$this->User_login_model->verifyUserNamePassword($userName,$password);
		if($userID){
			$sess_data=array(
				'userID'=>$userID,
				'userName'=>$userName
				);
			$this->session->set_userdata($sess_data);

			//print_r($this->session->session_id);

			echo json_encode(array('SessionID'=>$this->session->session_id));
		}
	}

	public function replaceUserDeviceInfo(){
		$userName=$this->input->post('userName');
		$password=$this->input->post('password');
		$board=$this->input->post('board');
		$bootloader=$this->input->post('bootloader');
		$brand=$this->input->post('brand');
		$device=$this->input->post('device');
		$display=$this->input->post('display');
		$hardware=$this->input->post('hardware');
		$manufacturer=$this->input->post('manufacturer');
		$model=$this->input->post('model');
		$product=$this->input->post('product');

		$flag_replaceUserDeviceInfo=$this->User_login_model->replaceUserDeviceInfo($userName,$password,$board,$bootloader,$brand,$device,$display,$hardware,$manufacturer,$model,$product);

	}

	public function isUserNameAvailable(){
		$userName=$this->input->post('userName');

		$this->User_login_model->isUserNameAvailable($userName);
	}

	public function isAlreadySignedIn(){
		$emailID=$this->input->post('emailID');

		$this->User_login_model->isAlreadySignedIn($emailID);
	}

	public function changePassword(){
		$sessionID=$this->input->post('sessionID');
		$oldPw=$this->input->post('oldPw');
		$newPw=$this->input->post('newPw');

		$this->User_login_model->changePassword($sessionID,$oldPw,$newPw);
	}

	public function logout(){
		$sessionID=$this->input->post('sessionID');

		$this->User_login_model->logout($sessionID);
	}
} 

?>