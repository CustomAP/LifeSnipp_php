<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Snippets extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Snippet_model');
	}
	
	public function getTimelineSnippets(){
		$sessionID=$this->input->post('sessionID');
		$userID=$this->input->post('userID');
		$num=$this->input->post('num');

		$this->Snippet_model->getTimelineSnippets($sessionID,$userID,$num);
	}

	public function shareSnippet(){
		$sessionID=$this->input->post('sessionID');
		$title=$this->input->post('title');
		$category=$this->input->post('category');
		$content=$this->input->post('content');
		$isAnonymous=$this->input->post('isAnonymous');

		date_default_timezone_set('America/Los_Angeles');

		$date = date('Y-m-d', time());
		$time =date('H:i:s',time());

		//echo $date."".$time;

		// $dateObj= new DateTime(shell_exec('date'));
		// $date= $dateObj->format('Y-m-d');
		// $time= $dateObj->format('H:i:s');

		$this->Snippet_model->shareSnippet($sessionID,$title,$category,$content,$date,$time,$isAnonymous);
	}

	public function getFeed(){
		$sessionID=$this->input->post('sessionID');
		$lowerLimit=$this->input->post('lowerLimit');

		$this->Snippet_model->getFeed($sessionID,$lowerLimit);
	}

	public function likeSnippet(){
		$sessionID=$this->input->post('sessionID');
		$postID=$this->input->post('snippetID');
		$like=$this->input->post('like');

		$this->Snippet_model->likeSnippet($sessionID,$postID,$like);
	}

	public function viewSnippet(){
		$sessionID=$this->input->post('sessionID');
		$postID=$this->input->post('snippetID');

		$this->Snippet_model->viewSnippet($sessionID,$postID);
	}

	public function getComments(){
		$sessionID=$this->input->post('sessionID');
		$postID=$this->input->post('snippetID');
		$num=$this->input->post('num');

		$this->Snippet_model->getComments($sessionID,$postID,$num);
	}

	public function comment(){
		$sessionID=$this->input->post('sessionID');
		$postID=$this->input->post('snippetID');
		$comment=$this->input->post('comment');

		date_default_timezone_set('America/Los_Angeles');

		$date = date('Y-m-d', time());
		$time =date('H:i:s',time());

		$this->Snippet_model->comment($sessionID,$postID,$comment,$date,$time);
	}

	public function editSnippet(){
		$sessionID=$this->input->post('sessionID');
		$postID=$this->input->post('snippetID');
		$title=$this->input->post('title');
		$category=$this->input->post('category');
		$content=$this->input->post('content');
		$isAnonymous=$this->input->post('isAnonymous');

		$this->Snippet_model->editSnippet($sessionID,$postID,$title,$category,$content,$isAnonymous);
	}

	public function deleteSnippet(){
		$sessionID=$this->input->post('sessionID');
		$postID=$this->input->post('snippetID');

		$this->Snippet_model->deleteSnippet($sessionID,$postID);
	}

	public function getSnippet(){
		$sessionID=$this->input->post('sessionID');
		$postID=$this->input->post('snippetID');

		$this->Snippet_model->getSnippet($sessionID,$postID);
	}

	public function getLikes(){
		$sessionID=$this->input->post('sessionID');
		$postID=$this->input->post('snippetID');
		$num=$this->input->post('num');

		$this->Snippet_model->getLikes($sessionID,$postID,$num);
	}

	public function editComment(){
		$sessionID=$this->input->post('sessionID');
		$comment=$this->input->post('comment');
		$commentID=$this->input->post('commentID');

		$this->Snippet_model->editComment($sessionID,$comment,$commentID);
	}

	public function deleteComment(){
		$sessionID=$this->input->post('sessionID');
		$commentID=$this->input->post('commentID');

		$this->Snippet_model->deleteComment($sessionID,$commentID);
	}
}