<?php

class User_profile_model extends CI_Model{

	public function getSelfProfileInfo($sessionID){

			$userName=$this->Session_data->getUserNameFromSessionID($sessionID);
			//$userName='saee';

			$this->load->model('User_login_model');

			$userID=$this->User_login_model->getUserIDForUserName($userName);


			$q=$this->db->select('user_info.firstName,user_info.lastName,user_info.bio,user_info.birthdate,user_info.following,user_info.followers,snippets.userID,SUM(snippets.views) as views,SUM(snippets.likes) as likes')
						->from('user_info')
						->join('snippets','snippets.userID = user_info.userID','left')
						->where('user_info.userID',$userID)
						->group_by('user_info.ID')
						->get();

			$row=$q->result();

			$q1=$this->db->select('emailID')
						 ->from('user_accounts')
						 ->where('userID',$userID)
						 ->get();

			$emailID=$q1->row()->emailID;

			echo json_encode(array('ProfileInfo'=>$row,'emailID'=>$emailID,'userID'=>$userID),JSON_FORCE_OBJECT);
	}


	public function getOthersProfileInfo($sessionID,$userID){
		
			$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

			$q=$this->db->select('user_info.*,user_accounts.userName,snippets.userID,SUM(snippets.views) as views,SUM(snippets.likes) as likes')
						->from('user_info')
						->join('user_accounts','user_info.userID = user_accounts.userID')
						->join('snippets','snippets.userID = user_info.userID','left')
						->where('user_info.userID',$userID)
						->group_by('user_info.ID')
						->get();

			$row=$q->result();

			$this->load->model('User_login_model');

			$selfUserID=$this->User_login_model->getUserIDForUserName($userName);

			$q1=$this->db->select('ID')
							->from('following')
							->where('followedBy',$selfUserID)
							->where('following',$userID)
							->get();

			$num_rows= $q1->num_rows();

			if($num_rows==0){
				$follow=0;
			}else{
				$follow=1;
			}

			echo json_encode(array('ProfileInfo'=>$row,'follow'=>$follow),JSON_FORCE_OBJECT);
	}

	public function addBasicInfo($sessionID,$gender,$country,$birthDate,$firstName,$lastName){

		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

		// $sessionID='ap';
		// $gender=1;
		// $country='ind';
		// $birthDate='1998-09-12';
		// $firstName='ash';
		// $lastName='pawar';

			$this->load->model('User_login_model');

			$userID=$this->User_login_model->getUserIDForUserName($userName);

			$data=array(
				'userID'=>$userID,
				'gender'=>$gender,
				'country'=>$country,
				'birthdate'=>$birthDate,
				'firstName'=>$firstName,
				'lastName'=>$lastName,
				'followers'=>0,
				'following'=>0,
				'bio'=>""
				);

			$q=$this->db->insert('user_info',$data);

			$error=$this->db->error();
			
			if($error['code'] != 00000)
				echo json_encode(array('ResultSet'=>1));
			else
				echo json_encode(array('ResultSet'=>0));
	}

	public function follow($sessionID,$userID){
		
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

			 // $userName='ap';
			 // $userID=20;

			$this->load->model('User_login_model');

			$selfUserID=$this->User_login_model->getUserIDForUserName($userName);

			if($selfUserID!=$userID){

				$q1=$this->db->select('ID')
							->from('following')
							->where('followedBy',$selfUserID)
							->where('following',$userID)
							->get();

				$num_rows= $q1->num_rows();

				if($num_rows==0){
					$data=array(
						'followedBy'=>$selfUserID,
						'following'=>$userID
						);

					$q=$this->db->insert('following',$data);

					$this->countFollowersFollowing($userID,$selfUserID);

					$notifData=array(
						'type'=>'follow',
						'userID'=>$userID,
						'sent_by'=>$selfUserID
						);

					$q2=$this->db->insert('notifications_users',$notifData);

					$error=$this->db->error();

					if($error['code']!=0000)
						echo json_encode(array('ResultSet'=>1));
					else
						echo json_encode(array('ResultSet'=>0));
				}
			}
	}

	public function unfollow($sessionID,$userID){
		
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

			// $sessionID='AshishPawar';
			// $userID=26;

			$this->load->model('User_login_model');

			$selfUserID=$this->User_login_model->getUserIDForUserName($userName);

			if($selfUserID!=$userID){

				$q1=$this->db->select('ID')
							->from('following')
							->where('followedBy',$selfUserID)
							->where('following',$userID)
							->get();

				$num_rows= $q1->num_rows();

				if($num_rows>0){
					$q=$this->db->where('following',$userID)
								->where('followedBy',$selfUserID)
								->delete('following');

					$this->countFollowersFollowing($userID,$selfUserID);

					$q1=$this->db->where('sent_by',$selfUserID)
								->where('userID',$userID)
								->delete('notifications_users');

					$error=$this->db->error();

					if($error['code']!=0000)
						echo json_encode(array('ResultSet'=>1));
					else
						echo json_encode(array('ResultSet'=>0));
				}
			}
	}


	private function countFollowersFollowing($userID,$selfUserID){
		$q2=$this->db->select('ID')
					->from('following')
					->where('followedBy',$selfUserID)
					->get();

		$num_rows_following=$q2->num_rows();

		$data_following=array(
		'following'=>$num_rows_following
		);

		$q3=$this->db->where('userID',$selfUserID)
					->update('user_info',$data_following);

		$q4=$this->db->select('ID')
					->from('following')
					->where('following',$userID)
					->get();

		$num_rows_followers=$q4->num_rows();

		$data_followers=array(
			'followers'=>$num_rows_followers
			);

		$q5=$this->db->where('userID',$userID)
					->update('user_info',$data_followers);
	}


	public function changeProfPic($sessionID,$image){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

			$this->load->model('User_login_model');

			$selfUserID=$this->User_login_model->getUserIDForUserName($userName);

			//$imageName='ash';
			$imageName=$selfUserID;

			$decodedImage=base64_decode($image);
			file_put_contents('./profile_pic/'.$imageName.".JPG", $decodedImage);

			echo json_encode(array('ResultSet'=>0));
			
	}

	public function editBio($sessionID,$bio){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

			$this->load->model('User_login_model');

			$userID=$this->User_login_model->getUserIDForUserName($userName);

			$data=array(
				'bio'=>$bio
				);

			$q=$this->db->where('userID',$userID)
						->update('user_info',$data);

			$error=$this->db->error();

			if($error['code']!=0000)
				echo json_encode(array('ResultSet'=>1));
			else
				echo json_encode(array('ResultSet'=>0));

	}

	public function editBirthDate($sessionID,$birthDate){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

			$this->load->model('User_login_model');

			$userID=$this->User_login_model->getUserIDForUserName($userName);

			$data=array(
				'birthdate'=>$birthDate
				);

			$q=$this->db->where('userID',$userID)
						->update('user_info',$data);

			$error=$this->db->error();

			if($error['code']!=0000)
				echo json_encode(array('ResultSet'=>1));
			else
				echo json_encode(array('ResultSet'=>0));
	}

	public function editDisplayName($sessionID,$firstName,$lastName){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);
		// $sessionID='ap';
		// $firstName='ash';
		// $lastName='pawar';

			$this->load->model('User_login_model');

			$userID=$this->User_login_model->getUserIDForUserName($userName);

			$data=array(
				'firstName'=>$firstName,
				'lastName'=>$lastName
				);

			$q=$this->db->where('userID',$userID)
						->update('user_info',$data);

			$error=$this->db->error();

			if($error['code']!=0000)
				echo json_encode(array('ResultSet'=>1));
			else
				echo json_encode(array('ResultSet'=>0));
	}

	public function getFollowers($sessionID,$num,$userID){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);
		//$userID=47;
		//$userName='saee';
		//$num=1;

		$this->load->model('User_login_model');

		if($userID==NULL)
		$userID=$this->User_login_model->getUserIDForUserName($userName);

		$selfUserID=$this->User_login_model->getUserIDForUserName($userName);

		$offset=10*$num;

		$q=$this->db->select('following.followedBy as UserID,user_accounts.userName,user_info.firstName,user_info.lastName')
					->from('following')
					->join('user_accounts','following.followedBy = user_accounts.userID','left')
					->join('user_info','following.followedBy = user_info.userID','left')
					->where('following.following',$userID)
					->order_by('user_info.firstName','inc')
					->limit(10,$offset)
					->group_by('user_info.firstName,user_info.lastName,user_accounts.userName,following.followedBy')
					->get();

		$result=$q->result();

		$result_array=$q->result_array();

		$num=$q->num_rows();

		$isFollowing=array();

		for($i=0;$i<$num;$i++){

		$q1=$this->db->select('count(ID) as isFollowing')
					->from('following')
					->where('following',$result_array[$i]['UserID'])
					->where('followedBy',$selfUserID)
					//->limit(10,$offset)
					->get();

		
		array_push($isFollowing, $q1->row());

		}

		$error=$this->db->error();

		echo json_encode(array('num'=>$num,'users'=>$result,'isFollowing'=>$isFollowing),JSON_FORCE_OBJECT);
	}


	public function getFollowing($sessionID,$num,$userID){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);
		//$userID=47;
		//$userName='saee';

		$this->load->model('User_login_model');

		if($userID==NULL)
		$userID=$this->User_login_model->getUserIDForUserName($userName);

		$selfUserID=$this->User_login_model->getUserIDForUserName($userName);

		$offset=10*$num;

		$q=$this->db->select('following.following as UserID,user_accounts.userName,user_info.firstName,user_info.lastName')
					->from('following')
					->join('user_accounts','following.following = user_accounts.userID','left')
					->join('user_info','following.following = user_info.userID','left')
					->where('following.followedBy',$userID)
					->order_by('user_info.firstName','inc')
					->limit(10,$offset)
					->group_by('user_info.firstName,user_info.lastName,user_accounts.userName,following.following')
					->get();

		$result=$q->result();

		$result_array=$q->result_array();

		$num=$q->num_rows();

		$isFollowing=array();

		for($i=0;$i<$num;$i++){

		$q1=$this->db->select('count(ID) as isFollowing')
					->from('following')
					->where('following',$result_array[$i]['UserID'])
					->where('followedBy',$selfUserID)
					//->limit(10,$offset)
					->get();

		
		array_push($isFollowing, $q1->row());

		}


		echo json_encode(array('num'=>$num,'users'=>$result,'isFollowing'=>$isFollowing),JSON_FORCE_OBJECT);
	}

	public function searchUsers($sessionID,$input){
		//$input='ap';
		$q=$this->db->select('user_accounts.userName,user_info.firstName,user_info.lastName,user_info.userID')
					->from('user_accounts')
					->join('user_info','user_info.userID = user_accounts.userID','left')
					->where("(user_accounts.userName LIKE '".$input."%')")
					->limit(10,0)
					->get();

		$result=$q->result();

		$num=$q->num_rows();

		echo json_encode(array('users'=>$result,'num'=>$num),JSON_FORCE_OBJECT);
					
	}
}

?>