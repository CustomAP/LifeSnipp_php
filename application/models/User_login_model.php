<?php

class User_login_model extends CI_Model{

	public function addUser($userName,$password,$emailID){

		$this->load->library('encryption');
		$data=array(
			'userName'=>$userName,
			'password'=>$this->encryption->encrypt($password),
			'emailID'=>$emailID
			);

		$q=$this->db->insert('user_accounts',$data);

		$q1=$this->db->select('userID')->from('user_accounts')->where('userName',$userName)->get();
		
		$error = $this->db->error();

		foreach($q1->result() as $object)
			$userID=$object->userID;
		
		if($error['code'] != 00000)
			echo json_encode(array('userID'=>0));
		else 
			echo json_encode(array('userID'=>$userID));

	}


	public function addUserDeviceInfo($userID,$board,$bootloader,$brand,$device,$display,$hardware,$manufacturer,$model,$product){
		$data=array(
			'userID'=>$userID,
			'board'=>$board,
			'bootloader'=>$bootloader,
			'brand'=>$brand,
			'display'=>$display,
			'hardware'=>$hardware,
			'manufacturer'=>$manufacturer,
			'model'=>$model,
			'product'=>$product
			);

		$q=$this->db->insert('userDeviceInfo',$data);

		$error=$this->db->error();

		if($error['code'] != 00000)
			echo json_encode(array('ResultSet'=>1));
		else 
			echo json_encode(array('ResultSet'=>0));
	}

	public function verifyUserNamePassword($userName,$password){

		$this->load->library('encryption');
		// $userName='ap';
		// $password='ashish';
		$q=$this->db->select('*')
					->from('user_accounts')
					->where(['userName'=>$userName])
					->get();

		$dbPassword=$q->row()->password;

		if(strcmp($this->encryption->decrypt($dbPassword),$password)==0){
			return $q->row()->userID;
		}else{
			return FALSE;
		}
	}

	public function replaceUserDeviceInfo($userName,$password,$board,$bootloader,$brand,$device,$display,$hardware,$manufacturer,$model,$product){
		
		if($this->doesUserNameExists($userName)){
		
			$userID=$this->verifyUserNamePassword($userName,$password);

			if($userID){
				$data=array(
					'board'=>$board,
					'bootloader'=>$bootloader,
					'brand'=>$brand,
					'device'=>$device,
					'display'=>$display,
					'hardware'=>$hardware,
					'manufacturer'=>$manufacturer,
					'model'=>$model,
					'product'=>$product
					);
		
				$q=$this->db->where('userID',$userID);
				$q=$this->db->update('userDeviceInfo',$data);

				$error=$this->db->error();

				if($error['code'] != 00000)
					echo json_encode(array('ResultSet'=>1));
				else 
					echo json_encode(array('ResultSet'=>0));
				}else{
					echo json_encode(array('ResultSet'=>2));
				}
			}else{
					echo json_encode(array('ResultSet'=>3));
			}
		}

	public function doesUserNameExists($userName){
		$q=$this->db->where(['userName'=>$userName])->get('user_accounts');
		
			if($q->num_rows()){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	


	public function getUserIDForUserName($userName){
		$q=$this->db->select('userID')
					->where(['userName'=>$userName])
					->get('user_accounts');

		return $q->row()->userID;
	}

	public function isUserNameAvailable($userName){

		$q=$this->db->select('userName')
					->from('user_accounts')
					->where('userName',$userName)
					->get();

		$count=$q->num_rows();

		if($count>0)
			echo json_encode(array('ResultSet'=>1));
		else if($count==0)
			echo json_encode(array('ResultSet'=>0));
	}


	public function isAlreadySignedIn($emailID){
		$q=$this->db->select('userID')
					->from('user_accounts')
					->where('emailID',$emailID)
					->get();

		$count=$q->num_rows();

		echo json_encode(array('ResultSet'=>$count));

	}

	public function changePassword($sessionID,$oldPw,$newPw){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

		$this->load->library('encryption');

			// $userName='ap';
			// $oldPw='ashish';
			// $newPw='ashish123';
			$q=$this->db->select('*')
						->from('user_accounts')
						->where('userName',$userName)
						->get();

			$dbPassword=$q->row()->password;

			if(strcmp($this->encryption->decrypt($dbPassword), $oldPw)==0){
				$data=array(
					'password'=>$this->encryption->encrypt($newPw)
					);

				$q1=$this->db->where('userName',$userName)
							->update('user_accounts',$data);

				$error=$this->db->error();

				if($error['code']!=0000)
					echo json_encode(array('ResultSet'=>2));
				else
					echo json_encode(array('ResultSet'=>0));
			}else{
				echo json_encode(array('ResultSet'=>1));
			}
	}

	public function logout($sessionID){

		echo json_encode(array('ResultSet'=>0));
	}
}
?>