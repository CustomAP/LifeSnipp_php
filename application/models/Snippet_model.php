<?php

class Snippet_model extends CI_Model{

	public function getTimelineSnippets($sessionID,$userID,$num){

		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

		  // $userName='ashish_pawar';
		  // $userID=47;
		  // $num=-1;

			$this->load->model('User_login_model');

			$selfUserID=$this->User_login_model->getUserIDForUserName($userName);

			if($userID==$selfUserID){
				$q=$this->db->select('snippets.*,count(likes.likeID) as isLiked')
							->from('snippets')
							->where('snippets.userID',$userID)
							->join('likes','likes.postID=snippets.postID AND likes.userID='.$selfUserID,'left')
							->group_by('snippets.postID')
							->get();

			}else{
				$offset=$num*10;
				$q=$this->db->select('snippets.*,count(likes.likeID) as isLiked')
							->from('snippets')
							->where('snippets.userID',$userID)
							->where('isAnonymous!=',1)
							->join('likes','likes.postID=snippets.postID AND likes.userID='.$selfUserID,'left')
							->limit(10,$offset)
							->group_by('snippets.postID')
							->get();
			}

			$row=$q->result();

			$num=$q->num_rows();

			$q1=$this->db->select('firstName,lastName,bio')
						 ->from('user_info')
						 ->where('userID',$userID)
						 ->get();

			$firstName=$q1->row()->firstName;
			$lastName=$q1->row()->lastName;
			$bio=$q1->row()->bio;

			echo json_encode(array('num'=>$num,'snippets'=>$row,'firstName'=>$firstName,'lastName'=>$lastName,'bio'=>$bio),JSON_FORCE_OBJECT);
	}


	public function shareSnippet($sessionID,$title,$category,$content,$date,$time,$isAnonymous){

		//echo "in share snippet";
		
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

			$this->load->model('User_login_model');

			$userID=$this->User_login_model->getUserIDForUserName($userName);

			$data=array(
				'userID'=>$userID,
				'title'=>$title,
				'category'=>$category,
				'content'=>$content,
				'uploadDate'=>$date,
				'uploadTime'=>$time,
				'views'=>0,
				'likes'=>0,
				'comments'=>0,
				'isAnonymous'=>$isAnonymous
				);

			$q=$this->db->insert('snippets',$data);

			$postID=$this->db->insert_id();

			$error=$this->db->error();

			if($error['code'] != 00000)
				echo json_encode(array('ResultSet'=>0));
			else 
				echo json_encode(array('ResultSet'=>$postID,'date'=>$date,'time'=>$time));
	}


	public function getFeed($sessionID,$lowerLimitDev){
		
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

			 // $userName='ashish_pawar';
			 // $lowerLimitDev=19;
			//$this->session->set_userdata('upperLimit',0);
			 //$lowerLimitDev=0;
			// $upperLimitDev=19;

			$this->load->model('User_login_model');

			$userID=$this->User_login_model->getUserIDForUserName($userName);

			$q1=$this->db->select_max('postID')
						->from('snippets')
						->get();

			$dbUpperLimit= $q1->row()->postID;

			//echo $this->session->userdata('upperLimit');

			// $upperLimit=0;
			// $lowerLimit=0;
			// if($upperLimitDev==0){

			// 	$this->session->set_userdata('upperLimit',$dbUpperLimit);
			// 	$this->session->set_userdata('lowerLimit',$dbUpperLimit-10);
				
			// 	//echo 'here';
			// 	$upperLimit=$dbUpperLimit;
			// 	$lowerLimit=$dbUpperLimit-10;

			// }else if(($dbUpperLimit-$this->session->userdata('upperLimit'))>50){

			// 	$this->session->set_userdata('$upperLimit',$dbUpperLimit);
			// 	$this->session->set_userdata('lowerLimit',$dbUpperLimit-10);

			// 	//echo 'here1';
			// 	$upperLimit=$dbUpperLimit;
			// 	$lowerLimit=$dbUpperLimit-10;

			// }else if(($dbUpperLimit-$this->session->userdata('upperLimit'))>10){

			// 	$lowerLimit=$this->session->userdata('upperLimit');
			// 	$upperLimit=$this->session->userdata('upperLimit')+10;

			// 	//echo 'here2';
			// 	$this->session->set_userdata('upperLimit',$dbUpperLimit-$this->session->userdata('upperLimit')+10);

			// }else{

			// 	$upperLimit=$this->session->userdata('lowerLimit');
			// 	$lowerLimit=$this->session->userdata('lowerLimit')-10;
			// 	//echo 'here3';	

			// 	if($upperLimitDev<$lowerLimit){
			// 		$upperLimit=$lowerLimitDev;
			// 		$lowerLimit=$lowerLimitDev-10;
			// 	}

			// 	$this->session->set_userdata('lowerLimit',$lowerLimit);
			// }


			if($lowerLimitDev==0){
				$lowerLimitDev=$dbUpperLimit;
			}

			// if($dbUpperLimit<10){
			// 	$lowerLimit=0;
			// }else{
			// 	$lowerLimit=$dbUpperLimit-10;
			// }

			// $upperLimit=$dbUpperLimit;

			$q=$this->db->select('snippets.*,IF(snippets.isAnonymous=0,user_info.firstName,"Anonymous") as firstName,IF(snippets.isAnonymous=0,user_info.lastName,"") as lastName,IF(snippets.isAnonymous=0,user_info.bio,"") as bio,count(likes.likeID) as isLiked,IF(snippets.isAnonymous=0,snippets.userID,0) as userID')
						->from('snippets')
						->where('snippets.postID >=',$lowerLimitDev-10)
						->where('snippets.postID <=',$lowerLimitDev)
						->where('snippets.userID !=',$userID)
						->join('user_info','snippets.userID=user_info.userID','left')
						->join('likes','likes.postID=snippets.postID and likes.userID='.$userID,'left')
						->group_by('snippets.postID,user_info.firstName,user_info.lastName,user_info.bio')
						->get();

			$row=$q->result();

			$num=$q->num_rows();

			$error=$this->db->error();

			echo json_encode(array('num'=>$num,'feed'=>$row),JSON_FORCE_OBJECT);
		}
	

	public function likeSnippet($sessionID,$postID,$like){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);


			// $userName='ap';
			// $postID=27;
			// $like=1;
			$this->load->model('User_login_model');

			$userID=$this->User_login_model->getUserIDForUserName($userName);

			if($like==1){

				$data=array(
					'userID'=>$userID,
					'postID'=>$postID
					);

				$q=$this->db->select('likeID')
							->from('likes')
							->where('userID',$userID)
							->where('postID',$postID)
							->get();

				$rows=$q->num_rows();

				if($rows==0){
					$q1=$this->db->insert('likes',$data);
				

				$q1=$this->db->select('likeID')
							->where('postID',$postID)
							->from('likes')
							->get();

				$likesCount=$q1->num_rows();

				$likesCountData=array(
					'likes'=>$likesCount
					);

				$q2=$this->db->where('postID',$postID)
							->update('snippets',$likesCountData);

				$q3=$this->db->select('userID')
							->from('snippets')
							->where('postID',$postID)
							->get();

				$postUserID=$q3->row()->userID;

				if($postUserID!=$userID){
					$notifData=array(
						'type'=>'like',
						'userID'=>$postUserID,
						'snippet_user_ID'=>$postID,
						'sent_by'=>$userID
						);
							
					$q4=$this->db->insert('notifications_snippet',$notifData);
				}

			}

			}else if($like==0){
				$q=$this->db->where('userID',$userID)
							->where('postID',$postID)
							->delete('likes');


				$q1=$this->db->select('likeID')
							->where('postID',$postID)
							->from('likes')
							->get();

				$likesCount=$q1->num_rows();

				$likesCountData=array(
					'likes'=>$likesCount
					);

				$q2=$this->db->where('postID',$postID)
							->update('snippets',$likesCountData);

				$q3=$this->db->where('sent_by',$userID)
							->where('snippet_user_ID',$postID)
							->delete('notifications_snippet');
			}

			$error=$this->db->error();

			if($error['code'] != 00000)
				echo json_encode(array('ResultSet'=>1));
			else
				echo json_encode(array('ResultSet'=>0));
	}


	public function viewSnippet($sessionID,$postID){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

			$this->load->model('User_login_model');

			$userID=$this->User_login_model->getUserIDForUserName($userName);

			$q=$this->db->select('userID')
						->from('snippets')
						->where('postID',$postID)
						->get();

			$snippetUserID=$q->row()->userID;

			if($snippetUserID!=$userID){
				$q1=$this->db->select('views')
							->from('snippets')
							->where('postID',$postID)
							->get();

				$views=$q1->row()->views;

				$views=$views+1;

				$data=array(
					'views'=>$views
					);
				$q2=$this->db->where('postID',$postID)
							->update('snippets',$data);
			}

	}

	public function getComments($sessionID,$postID,$num){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);
			
			 // $userName='ashish';
			 // $postID=69;
			 // $num=0;

			$offset=$num*10;

			$this->load->model('User_login_model');

			$userID=$this->User_login_model->getUserIDForUserName($userName);

			$offset=$num*10;

			$q1=$this->db->select('userID')
						->from('snippets')
						->where('postID',$postID)
						->get();

			$postUserID=$q1->row()->userID;

			if($userID!=$postUserID){
				$q=$this->db->select('comments.commentID,comments.comment,comments.commentDate,comments.commentTime,comments.postID,IF((select snippets.isAnonymous from snippets where snippets.postID='.$postID.')=1 AND (select snippets.userID = comments.userID) AND (comments.userID != '.$userID.'),0,comments.userID) AS userID,IF((select snippets.isAnonymous from snippets where snippets.postID='.$postID.')=1 AND (select snippets.userID = comments.userID),"Anonymous",user_info.firstName) as firstName,IF((select snippets.isAnonymous from snippets where snippets.postID='.$postID.')=1 AND (select snippets.userID = comments.userID),"",user_info.lastName) as lastName')
				        ->from('comments')
						->join('user_info','user_info.userID = comments.userID','left')
						->join('snippets','snippets.postID = comments.postID','left')
						->where('comments.postID',$postID)
						->order_by('comments.commentID','desc')
						->limit(10,$offset)
						->group_by('comments.commentID,user_info.firstName,user_info.lastName')
						->get();
					}else{
						$q=$this->db->select('comments.*,user_info.firstName,user_info.lastName,user_info.userID')
									->from('comments')
									->join('user_info','user_info.userID = comments.userID','left')
									->join('snippets','snippets.postID = comments.postID','left')
									->where('comments.postID',$postID)
									->order_by('comments.commentID','desc')
									->limit(10,$offset)
									->group_by('comments.commentID,user_info.firstName,user_info.lastName')
									->get();
					}

			$result=$q->result();

			$num=$q->num_rows();

			$error=$this->db->error();

			echo json_encode(array('comments'=>$result,'num'=>$num),JSON_FORCE_OBJECT);
	}

	public function comment($sessionID,$postID,$comment,$date,$time){
		
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

		$this->load->model('User_login_model');

		$userID=$this->User_login_model->getUserIDForUserName($userName);

		$data=array(
			'comment'=>$comment,
			'commentDate'=>$date,
			'commentTime'=>$time,
			'postID'=>$postID,
			'userID'=>$userID
			);

		$q=$this->db->insert('comments',$data);

		$q1=$this->db->select('commentID')
					->where('postID',$postID)
					->from('comments')
					->get();

		$commentsCount=$q1->num_rows();

		$likesCountData=array(
			'comments'=>$commentsCount
			);

		$q2=$this->db->where('postID',$postID)
					->update('snippets',$likesCountData);

		$q3=$this->db->select('userID')
					->from('snippets')
					->where('postID',$postID)
					->get();

		$postUserID=$q3->row()->userID;

		if($postUserID!=$userID){
			$notifData=array(
				'type'=>'comment',
				'userID'=>$postUserID,
				'snippet_user_ID'=>$postID,
				'sent_by'=>$userID
				);

			$q4=$this->db->insert('notifications_snippet',$notifData);
		}

		$error=$this->db->error();

		if($error['code'] != 00000)
			echo json_encode(array('ResultSet'=>1));
		else
			echo json_encode(array('ResultSet'=>0));
	}


	public function editSnippet($sessionID,$postID,$title,$category,$content,$isAnonymous){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

			// $userName='ap'; 
			// $postID=47;
			// $title='title';
			// $content='content';
			// $category='cat';
			$this->load->model('User_login_model');

			$selfUserID=$this->User_login_model->getUserIDForUserName($userName);

			$q1=$this->db->select('userID')
						->from('snippets')
						->where('postID',$postID)
						->get();

			$userID=$q1->row()->userID;

			if($selfUserID==$userID){
				$data=array(
					'title'=>$title,
					'category'=>$category,
					'content'=>$content,
					'isAnonymous'=>$isAnonymous
					);

				$q=$this->db->where('postID',$postID)
							->update('snippets',$data);

				$error=$this->db->error();

				if($error['code'] != 00000)
					echo json_encode(array('ResultSet'=>1));
				else
					echo json_encode(array('ResultSet'=>0));
			}
	}

	public function deleteSnippet($sessionID,$postID){
		
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

			// $sessionID='AshishPawar';
			// $postID=5;

			$this->load->model('User_login_model');

			$selfUserID=$this->User_login_model->getUserIDForUserName($userName);

			$q1=$this->db->select('userID')
						->from('snippets')
						->where('postID',$postID)
						->get();

			$userID=$q1->row()->userID;

			if($selfUserID==$userID){

				$q1=$this->db->where('postID',$postID)
							->delete('likes');

				$q2=$this->db->where('postID',$postID)
							->delete('comments');

				$q=$this->db->where('postID',$postID)
							->delete('snippets');

				$error=$this->db->error();
				
				if($error['code'] != 00000)
					echo json_encode(array('ResultSet'=>1));
				else
					echo json_encode(array('ResultSet'=>0));			
			}
	}

	public function getSnippet($sessionID,$postID){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);
		 // $postID=37;
		 // $userName='ap';
			$this->load->model('User_login_model');
			$userID=$this->User_login_model->getUserIDForUserName($userName);

			$q1=$this->db->select('userID')
						->from('snippets')
						->where('postID',$postID)
						->get();

			$postUserID=$q1->row()->userID;

			if($userID!=$postUserID){
				$q=$this->db->select('snippets.*,IF(snippets.isAnonymous=0,user_info.firstName,"Anonymous") as firstName,IF(snippets.isAnonymous=0,user_info.lastName,"") as lastName,IF(snippets.isAnonymous=0,user_info.bio,"") as bio,count(likes.likeID) as isLiked,IF(snippets.isAnonymous=0,snippets.userID,0) as userID')
						->from('snippets')
						->where('snippets.postID',$postID)
						->join('user_info','snippets.userID=user_info.userID','left')
						->join('likes','likes.postID=snippets.postID and likes.userID='.$userID,'left')
						->group_by('snippets.postID,user_info.firstName,user_info.lastName,user_info.bio')
						->get();

			}else{
				$q=$this->db->select('snippets.*,user_info.firstName,user_info.lastName,user_info.bio,count(likes.likeID) as isLiked')
							->from('snippets')
							->where('snippets.postID',$postID)
							->join('user_info','snippets.userID=user_info.userID','left')
							->join('likes','likes.postID=snippets.postID and likes.userID='.$userID,'left')
							->group_by('snippets.postID,user_info.firstName,user_info.lastName,user_info.bio')
							->get();
			}

			$result=$q->result();

			$error=$this->db->error();

			echo json_encode($result,JSON_FORCE_OBJECT);
		}


	public function getLikes($sessionID,$postID,$num){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);
		$this->load->model('User_login_model');

		$selfUserID=$this->User_login_model->getUserIDForUserName($userName);

		// $postID=19;
		// $sessionID='ap';
		// $selfUserID=26;
		
		$q=$this->db->select('snippets.userID')
					->from('snippets')
					->where('postID',$postID)
					->get();

		$userID=$q->row()->userID;

		//xecho $userID;
		$offset=$num*10;

		//if($userID==$selfUserID){
			$q1=$this->db->select('likes.userID as UserID,user_accounts.userName,user_info.firstName,user_info.lastName')
						->from('likes')
						->where('likes.postID',$postID)
						->join('user_accounts','user_accounts.userID = likes.userID','left')
						->join('user_info','user_info.userID = likes.userID','left')
						->group_by('user_info.firstName,user_info.lastName,user_accounts.userName,likes.userID')
						->get();

			$result=$q1->result();

			$result_array=$q1->result_array();

			$num_rows=$q1->num_rows();

			$isFollowing=array();

			for($i=0;$i<$num_rows;$i++){

				$q2=$this->db->select('count(ID) as isFollowing')
					->from('following')
					->where('following',$result_array[$i]['UserID'])
					->where('followedBy',$selfUserID)
					// ->limit(10,$offset)
					->get();

		
				array_push($isFollowing, $q2->row());

			}

			$error=$this->db->error();

			echo json_encode(array('num'=>$num_rows,'users'=>$result,'isFollowing'=>$isFollowing),JSON_FORCE_OBJECT);
		//}
	}

	public function editComment($sessionID,$comment,$commentID){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

		// $userName='ap';
		// $comment='abb';
		// $commentID=91;

		$this->load->model('User_login_model');
		
		$userID=$this->User_login_model->getUserIDForUserName($userName);

		$q=$this->db->select('comments.userID')
					->from('comments')
					->where('comments.commentID',$commentID)
					->get();

		if($userID==$q->row()->userID){
			$data=array(
				'comment'=>$comment
				);

			$q1=$this->db->where('commentID',$commentID)
						->update('comments',$data);
			
			$error=$this->db->error();

			if($error['code']!=0000)
				echo json_encode(array('ResultSet'=>1));
			else
				echo json_encode(array('ResultSet'=>0));
		}
	}

	public function deleteComment($sessionID,$commentID){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

		// $userName='ap';
		// $commentID=88;
		$this->load->model('User_login_model');

		$userID=$this->User_login_model->getUserIDForUserName($userName);

		$q=$this->db->select('comments.userID')
					->from('comments')
					->where('comments.commentID',$commentID)
					->get();

		//print_r($q->result());

		if($userID==$q->row()->userID){

			$q3=$this->db->select('postID')
						->from('comments')
						->where('commentID',$commentID)
						->get();

			$postID=$q3->row()->postID;

			$q1=$this->db->where('commentID',$commentID)
						->delete('comments');

			$q2=$this->db->select('commentID')
					->where('postID',$postID)
					->from('comments')
					->get();

			$commentsCount=$q2->num_rows();

			$commentsCountData=array(
			'comments'=>$commentsCount
			);

			$q3=$this->db->where('postID',$postID)
						->update('snippets',$commentsCountData);

			$error=$this->db->error();

			if($error['code']!=0000)
				echo json_encode(array('ResultSet'=>1));
			else
				echo json_encode(array('ResultSet'=>0));

		}

	}
}