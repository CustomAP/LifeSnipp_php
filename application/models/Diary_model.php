<?php

class Diary_model extends CI_Model{

	public function getDiarySnippets($sessionID){

		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

			$this->load->model('User_login_model');

			$userID=$this->User_login_model->getUserIDForUserName($userName);

			$q=$this->db->select('*')
					 ->from('diarySnippets')
					 ->where('userID',$userID)
					 ->get();

			$row=$q->result();

			$num=$q->num_rows();

			echo json_encode(array('num'=>$num,'diarySnippets'=>$row),JSON_FORCE_OBJECT);
	}


	public function addToDiary($sessionID,$title,$category,$content,$date,$time){

		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

			$this->load->model('User_login_model');

			$userID=$this->User_login_model->getUserIDForUserName($userName);

			$data=array(
				'userID'=>$userID,
				'title'=>$title,
				'category'=>$category,
				'content'=>$content,
				'uploadDate'=>$date,
				'uploadTime'=>$time
				);

			$q=$this->db->insert('diarySnippets',$data);

			$snippetID=$this->db->insert_id();

			$error=$this->db->error();

			if($error['code'] != 00000)
				echo json_encode(array('ResultSet'=>0));
			else 
				echo json_encode(array('ResultSet'=>$snippetID,'date'=>$date,'time'=>$time));
	}

	public function editDiarySnippet($sessionID,$title,$category,$content,$snippetID){
		
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

		 // $userName='ap';
		 // $title='test';
		 // $category='test';
		 // $content='test';
		 // $snippetID=13;
			$this->load->model('User_login_model');

			$selfUserID=$this->User_login_model->getUserIDForUserName($userName);

			$q=$this->db->select('userID')
						->from('diarySnippets')
						->where('snippetID',$snippetID)
						->get();

			$userID=$q->row()->userID;

			if($selfUserID==$userID){
				$data=array(
					'title'=>$title,
					'category'=>$category,
					'content'=>$content
					);

				$q=$this->db->where('snippetID',$snippetID)
							->update('diarySnippets',$data);

				$error=$this->db->error();

				if($error['code'] != 00000)
					echo json_encode(array('ResultSet'=>1));
				else
					echo json_encode(array('ResultSet'=>0));
			}
	}

	public function deleteDiarySnippet($sessionID,$snippetID){
		
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

			 // $userName='ap';
			 // $snippetID=10;

			$this->load->model('User_login_model');

			$selfUserID=$this->User_login_model->getUserIDForUserName($userName);

			$q=$this->db->select('userID')
						->from('diarySnippets')
						->where('snippetID',$snippetID)
						->get();

			$userID=$q->row()->userID;

			if($selfUserID==$userID){
				$q1=$this->db->where('snippetID',$snippetID)
							->delete('diarySnippets');

				$error=$this->db->error();

				if($error['code'] != 00000)
					echo json_encode(array('ResultSet'=>1));
				else
					echo json_encode(array('ResultSet'=>0));
			}
	}
}