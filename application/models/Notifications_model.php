<?php

class Notifications_model extends CI_Model{

	public function getNotifications($sessionID){
		$userName=$this->Session_data->getUserNameFromSessionID($sessionID);

		 //$userName='tmw98';
		  //$userID=37;

		$this->load->model('User_login_model');

		$userID=$this->User_login_model->getUserIDForUserName($userName);


		/*
			getting notifications related to snippets
		*/

		$q=$this->db->distinct()
					->select('snippet_user_ID,type')
					->from('notifications_snippet')
					->where('userID',$userID)
					->get();

		$result=$q->result_array();

		//print_r($result);

		$num_rows_snippets=$q->num_rows();

		$notifications=array();
		$snippet_user_IDs=array();
		$type=array();

		for($i=0;$i<$num_rows_snippets;$i++){
			$q1=$this->db->select('user_info.firstName,user_info.lastName,notifications_snippet.*')
						->from('notifications_snippet')
						->join('user_info','notifications_snippet.sent_by = user_info.userID','left')
						->where('notifications_snippet.snippet_user_ID',$result[$i]['snippet_user_ID'])
						->where('notifications_snippet.type',$result[$i]['type'])
						->get();

			$notif_info=$q1->result_array();

			$notif='';

			if($q1->num_rows()<=4){

				for($j=0;$j<$q1->num_rows();$j++){
					if($j==$q1->num_rows()-1&&$q1->num_rows()>=2)
						$notif=$notif.' and ';
					else if($j<$q1->num_rows()-1 && $q1->num_rows()>=2 && $j!=0)
						$notif=$notif.', ';
					$notif=$notif.$notif_info[$j]['firstName'].' '.$notif_info[$j]['lastName'];
				}

				if($result[$i]['type']=='like'){
					$notif=$notif.' liked your snippet.';
				}else if($result[$i]['type']=='comment'){
					$notif=$notif.' commented on your snippet.';
				}

			}else{
				$num=$q1->num_rows();
				$num=$num-2;

				$lastPhrase='';

				if($result[$i]['type']=='like')
					$lastPhrase=' others liked your snippet.';
				else if($result[$i]['type']=='comment')
					$lastPhrase=' others commented on your snippet.';	

				for($j=0;$j<2;$j++){
					if($j==1)
						$notif=$notif.', ';
					$notif=$notif.$notif_info[$j]['firstName'].' '.$notif_info[$j]['lastName'];
				}

				$notif=$notif.' and '.$num.' '.$lastPhrase;
			}

			$notifications[$i]=$notif;

			$snippet_user_IDs[$i]=$result[$i]['snippet_user_ID'];
			$type[$i]=$result[$i]['type'];
			//print_r($notif);
		}


		/*
			getting notifications related to users
		*/
		$q2=$this->db->select('*')
					->from('notifications_users')
					->where('userID',$userID)
					->get();

		$result_users=$q2->result_array();

		$num_rows_users=$q2->num_rows();


		for($i=0;$i<$num_rows_users;$i++){

			$q3=$this->db->select('user_info.firstName,user_info.lastName,notifications_users.*')
						->from('notifications_users')
						->join('user_info','notifications_users.sent_by = user_info.userID','left')
						->where('notifications_users.userID',$result_users[$i]['userID'])
						->get();

			$notif_info=$q3->result_array();

			$notifications[$num_rows_snippets+$i]=$notif_info[$i]['firstName'].' '.$notif_info[$i]['lastName'].' started following you.';

			$snippet_user_IDs[$num_rows_snippets+$i]=$result_users[$i]['sent_by'];
			$type[$num_rows_snippets+$i]=$result_users[$i]['type'];
		}


		echo json_encode(array('num'=>$num_rows_snippets+$num_rows_users,'notifications'=>$notifications,'type'=>$type,'snippet_user_ID'=>$snippet_user_IDs),JSON_FORCE_OBJECT);

		for($i=0;$i<$num_rows_snippets;$i++){
			$q4=$this->db->where('snippet_user_ID',$result[$i]['snippet_user_ID'])
						->delete('notifications_snippet');
		}
		for($i=0;$i<$num_rows_users;$i++){
			$q5=$this->db->where('ID',$result_users[$i]['ID'])
						->delete('notifications_users');
		}
	}
}

?>