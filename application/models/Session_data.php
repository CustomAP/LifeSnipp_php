<?php

class Session_data extends CI_Model{

	public function getUserNamefromSessionID($sessionID){

	 // $sessionID='4a3db61f37487efbcb36f33c46f1b38fb916005a';

		$q=$this->db->select('data')
						->from('ci_sessions')
						->where('id',$sessionID)
						->get();

			$row=$q->row();

			$session_data = $row->data ;  // your BLOB data who are a String

   			$return_data = array();  // array where you put your "BLOB" resolved data
             
   			$offset = 0;
   			
   			while ($offset < strlen($session_data)){
       			if (!strstr(substr($session_data, $offset), "|")) {
          			throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
       			 }
         		$pos = strpos($session_data, "|", $offset);
          		$num = $pos - $offset;
          		$varname = substr($session_data, $offset, $num);
          		$offset += $num + 1;
          		$data = unserialize(substr($session_data, $offset));
          		$return_data[$varname] = $data;  
          		$offset += strlen(serialize($data));
      		}

      	$userName=$return_data['userName'];

      	return $userName;
	}
} 

?>