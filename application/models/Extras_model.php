<?php

class Extras_model extends CI_Model{


	public function checkUpdate($sessionID,$appVersion){
		$latestVersion='1.0.3';
		if($latestVersion==$appVersion){
			echo json_encode(array('toUpdate'=>0,'isMandatory'=>0));
		 }else{
		 	echo json_encode(array('toUpdate'=>1,'isMandatory'=>1));			
		 }
	}


	public function getCategories($sessionID){
			
			$categories=array(
				'0'=>'sadMoment',
				'1'=>'embarrassingMoment',
				'2'=>'happyMoment',
				'3'=>'collegeLife',
				'4'=>'schoolLife',
				'5'=>'happyDays',
				'6'=>'toughDays',
				'7'=>'hardTimes',
				'8'=>'cleverMoment',
				'9'=>'shockingMoment',
				'10'=>'unexpected',
				'11'=>'memorable',
				'12'=>'love',
				'13'=>'sacrifice',
				'14'=>'hate',
				'15'=>'perfectTiming',
				'16'=>'lucky',
				'17'=>'hardLuck',
				'18'=>'lonely',
				'19'=>'friends',
				'20'=>'family',
				'21'=>'kindAct',
				'22'=>'sympathy',
				'23'=>'goodDeed',
				'24'=>'karma',
				'25'=>'broCode',
				'26'=>'sadist',
				'27'=>'lit',
				'28'=>'joy',
				'29'=>'sorrow',
				'30'=>'repay',
				'31'=>'oldGoodDays',
				'32'=>'howPeopleChange',
				'33'=>'rudeBehaviour',
				'34'=>'stunned',
				'35'=>'tears',
				'36'=>'misunderstanding',
				'37'=>'regret',
				'38'=>'relationship',
				'39'=>'singleLife',
				'40'=>'parents',
				'41'=>'thugLife',
				'42'=>'pain',
				'43'=>'bff',
				'44'=>'bros4life',
				'45'=>'job',
				'46'=>'cops',
				'47'=>'confession',
				'48'=>'digitalLife',
				'49'=>'nostalgia',
				'50'=>'propose',
				'51'=>'funnyMoment'
				);

			$num=count($categories);

			echo json_encode(array('num'=>$num,'categories'=>$categories),JSON_FORCE_OBJECT);
	}
}